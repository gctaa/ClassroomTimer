# Classroom Timer

This is a project of students in a web development course at the [Arlington
Career Center](https://careercenter.apsva.us/) to create an activity time for
use in our classrooms using the HTML5, CSC3, and JavaScript tools we are
learning in class.
